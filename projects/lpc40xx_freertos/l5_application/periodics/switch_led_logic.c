#include "switch_led_logic.h"

// Initializes a specific switch by enabling its pull-down resistors.

// This setup ensures the switch is properly configured before use.
void switch_led_logic__initialize(gpio_s gpio_switch) { gpio__enable_pull_down_resistors(gpio_switch); }

// Operates a specific LED and switch in a single run.

// If the switch is detected to be at a high level (i.e., pressed), it performs an action on the LED.

// This function turns the LED on when the switch is pressed and off when it is not.
void switch_led_logic__run_once(gpio_s gpio_led, gpio_s gpio_switch) {
  if (gpio__get(gpio_switch)) {
    gpio__set(gpio_led);
  } else {
    gpio__reset(gpio_led);
  }
}