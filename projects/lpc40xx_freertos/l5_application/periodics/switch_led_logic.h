#ifndef SWITCH_LED_LOGIC_H
#define SWITCH_LED_LOGIC_H

#include "board_io.h"
#include "gpio.h"

void switch_led_logic__initialize(gpio_s gpio_switch);
void switch_led_logic__run_once(gpio_s gpio_led, gpio_s gpio_switch);

#endif // SWITCH_LED_LOGIC_H